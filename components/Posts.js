import React from "react";
import {Text, View, Image, StyleSheet } from "react-native";

import {fetchMainPosts} from "../utils/api";
import Loading from "./Loading";
import PostsList from "./PostsList";

function postsReducer (state, action) {
  if (action.type === 'fetch') {
    return {
      posts: null,
      error: null,
      loading: true
    };
  } else if (action.type === 'success') {
    return {
      posts: action.posts,
      error: null,
      loading: false,
    };
  } else if (action.type === 'error') {
    return {
      posts: state.posts,
      error: action.message,
      loading: false
    };
  } else {
    throw new Error(`That action type is not supported.`);
  }
}

const Posts = ({navigation}) => {
  const [state, dispatch] = React.useReducer(
    postsReducer,
    { posts: null, error: null, loading: true }
  );
  React.useEffect(() => {
    dispatch({ type: 'fetch' });
    
    fetchMainPosts('top')
      .then((posts) => dispatch({ type: 'success', posts }))
      .catch(({ message }) => dispatch({ type: 'error', error: message }))
  }, []);
  
  if (state.loading === true) {
    return <Loading />;
  }
  if (state.error) {
    return <Text>{state.error}</Text>
  }
  return (
    <>
      <View style={styles.imgContainer}>
        <Image
          style={styles.tinyLogo}
          source={{
            uri: 'https://reactnative.dev/img/tiny_logo.png',
          }}
        />
      </View>
      <PostsList posts={state.posts} navigation={navigation}/>
    </>
    
  );
}

const styles = StyleSheet.create({
  imgContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 5,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
});

export default Posts;
