import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Button, } from 'react-native';


const PostsList = ({posts, navigation}) => {
  const Item = ({ title, id, url}) => (
    <View style={styles.item}>
      <Button
        style={styles.title}
        title={title}
        onPress={() => {
          // Navigate to the Details route with params
          navigation.navigate('Details Story', {
            itemId: id,
            title: title,
            url: url,
          });
        }}
      />
    </View>
  );
  const renderItem = ({ item }) => (
    <Item title={item.title} id={item.id} url={item.url}/>
  );
  // VirtualizedList: A large list that is slow to update
  const memoizedValue = React.useMemo(() => renderItem, [posts]);
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={posts}
        renderItem={memoizedValue}
        keyExtractor={item => String(item.id)}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 3,
    marginVertical: 6,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default PostsList;
