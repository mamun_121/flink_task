import React from 'react';
import {Text} from 'react-native';
import { WebView } from 'react-native-webview';
import { StyleSheet } from 'react-native';
import Constants from 'expo-constants';

import {fetchItemDetails} from "../utils/api";
import Loading from "./Loading";


const ItemsDetails = ({ route }) => {
  const [data, setData] = React.useState(null);
  let listRef = React.useRef(null);
  const { itemId, url, title } = route.params;
  
  React.useEffect(()=> {
    fetchItemDetails(itemId)
      .then((data) => setData(data))
  }, [itemId]);
  
  if (data === null) {
    return <Loading />;
  }
  
  return (
    <>
      <Text style={styles.titleText}>{title}</Text>
      <WebView
        style={styles.container}
        source={{ uri: url }}
        ref={(ref) => {
          listRef = ref;
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  titleText: {
    color: '#e01515',
    fontWeight: "bold",
    fontSize: 28,
    textAlign: 'center',
  },
});


export default ItemsDetails;
